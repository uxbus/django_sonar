#      _____  _____  __ __  _____  _____  _____  _____
#     |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#     |   __||     ||_   _||     ||   __||   __||     |
#     |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#
#     ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#     Copyright (c) 2019. All rights reserved.
#
from django.conf import settings
import re
import os
import logging


logger = logging.getLogger(__name__)


def get_local_packages():
    """
    use this to get a list of local packages for coverage and test report handling-
    we don't use django.apps.apps as we want all folders that may run code on the project.

    :return: list of folder names that contain a __init__.py file
    """

    logger.debug("get_local_packages base: %s", settings.BASE_DIR)

    ret = []

    for subdir in next(os.walk(settings.BASE_DIR))[1]:
        if os.path.exists(os.path.join(settings.BASE_DIR, subdir, '__init__.py')):
            ret.append(subdir)

    return ret


def replace_coverage_source(report_path, value):
    """
    as there have been some troubles about source directories on report files
    generated with coverage.py this ensures all source tags are set to
    settings.BASE_DIR.

    problems have been with path from coverage.py: /usr/home/someone/workspace-python/somproj …
    but sonar expected path to be /home/someone/workspace-python/somproj

    :param report_path: path to coverage-report to replace
    :return: None
    """

    replacement = value if isinstance(value, str) else settings.BASE_DIR

    logger.debug("replace_coverage_source report_path: %s replacement; %s", report_path, replacement)

    with open(report_path, 'r') as coverage_report_file:
        file_content = coverage_report_file.read()
        file_content = re.sub(
            "<source>.*</source>",
            "<source>{}</source>".format(replacement),
            file_content,
        )

    with open(report_path, "w") as file:
        file.write(file_content)
