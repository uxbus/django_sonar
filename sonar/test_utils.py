#      _____  _____  __ __  _____  _____  _____  _____
#     |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#     |   __||     ||_   _||     ||   __||   __||     |
#     |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#
#     ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#     Copyright (c) 2019. All rights reserved.
#
from sonar.utils import get_local_packages, replace_coverage_source
from bs4 import BeautifulSoup
import os


def test_get_local_packages():
    packages = get_local_packages()
    assert 2 == len(packages)
    assert 'sonar' in packages
    assert 'django_sonar' in packages


def test_replace_coverage_source(tmpdir, settings):

    fh = tmpdir.join("coverage-test.xml")
    fh.write(open(os.path.join(settings.BASE_DIR, "resources/coverage.xml")).read())
    print("testing with", fh)

    replace_coverage_source(fh, True)

    with open(fh, 'r') as coverage_report_file:
        soup = BeautifulSoup(coverage_report_file, "lxml")
        for source in soup.find('sources').find_all('source'):
            assert settings.BASE_DIR == source.get_text()


def test_replace_coverage_source_val(tmpdir, settings):

    fh = tmpdir.join("coverage-test.xml")
    fh.write(open(os.path.join(settings.BASE_DIR, "resources/coverage.xml")).read())
    print("testing with", fh)

    replace_coverage_source(fh, 'HUAHUA')

    with open(fh, 'r') as coverage_report_file:
        soup = BeautifulSoup(coverage_report_file, "lxml")
        for source in soup.find('sources').find_all('source'):
            assert 'HUAHUA' == source.get_text()
