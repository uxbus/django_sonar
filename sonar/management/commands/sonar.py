#      _____  _____  __ __  _____  _____  _____  _____
#     |__   ||  _  ||  |  ||  _  ||__   ||__   ||  _  | .DE
#     |   __||     ||_   _||     ||   __||   __||     |
#     |_____||__|__|  |_|  |__|__||_____||_____||__|__| GMBH
#
#     ZAYAZZA PROPRIETARY/CONFIDENTIAL.
#     Copyright (c) 2019. All rights reserved.
#
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from sonar.utils import get_local_packages, replace_coverage_source
import configparser
import shutil
import subprocess
import os
import traceback


class Command(BaseCommand):
    help = 'prepare and send test-results and coverage to sonarqube'

    """
    wrote this command to handle various problems that occured on using sonar:
     * cleanup of old coverage data
     * coverage data doesn't show up in sonarqube
     * cleanup of old test-reports and place them to default location
     
    problems seen so var with coverage-data:
     * pytest-cov / the combination of coverage.xml/sources/source + coverage.xml/packages/package/classes/class@fileanme
       must point to an existing file (somethings the package prefix is missing)
     * java always fails on symlinks! they occured in coverage.xml/sources/source 
     * problems have been with path from coverage.py which was /usr/home/someone/workspace-python/somproj … but sonar
       expected path to be /home/someone/workspace-python/somproj
    
    to find more information: https://community.sonarsource.com/t/sonar-scanner-does-not-extract-python-coverage-measures-on-codeship-but-works-on-local/869
    java class that does the coverage analyzing: https://github.com/SonarSource/sonar-python/blob/1.10.0.2131/sonar-python-plugin/src/main/java/org/sonar/plugins/python/coverage/CoberturaParser.java#L62
    """

    def handle(self, *args, **options):
        test_results_dir = getattr(settings, 'SONAR_TEST_REPORTS',  'xunit-reports')
        coverage_results_dir = getattr(settings, 'SONAR_COVERAGE_REPORTS',  'coverage-reports')

        config = configparser.ConfigParser()
        config.read(os.path.join(settings.BASE_DIR, 'pytest.ini'))
        settings_module = config.get('pytest', 'DJANGO_SETTINGS_MODULE')

        sub_env = os.environ.copy()
        sub_env['DJANGO_SETTINGS_MODULE'] = settings_module

        try:
            # remove old results
            if os.path.exists(os.path.join(settings.BASE_DIR, test_results_dir)):
                shutil.rmtree(os.path.join(settings.BASE_DIR, test_results_dir))

            if os.path.exists(os.path.join(settings.BASE_DIR, coverage_results_dir)):
                shutil.rmtree(os.path.join(settings.BASE_DIR, coverage_results_dir))

            packages = get_local_packages()

            for package_name in packages:

                test_repport_path = os.path.join(test_results_dir, "xunit-result-{}.xml".format(package_name))
                coverage_report_path = os.path.join(coverage_results_dir, "coverage-{}.xml".format(package_name))

                cmd = "coverage erase"
                print(cmd)
                subprocess.run(cmd.split(' '), env=sub_env)

                # this maid not work on windows or mac …
                cmd = "python -m pytest {} --junitxml={} --cov={} --cov-report=xml:{}".format(
                    package_name,
                    test_repport_path,
                    package_name,
                    coverage_report_path
                )
                print(cmd)
                subprocess.run(cmd.split(' '), env=sub_env)

                # as pytest-cov had problems on creating a valid report we use coverage.py directly and
                # overide ptest-cov results
                cmd = "coverage xml -i -o {}".format(coverage_report_path)
                print(cmd)
                subprocess.run(cmd.split(' '), env=sub_env)

                # replace source if needed
                source_rep = getattr(settings, 'SONAR_COVERAGE_SOURCE_TO_BASE_DIR', None)
                if source_rep is not None:
                    replace_coverage_source(os.path.join(settings.BASE_DIR, coverage_report_path), source_rep)

            # NOTE: for debugging add - that watch for
            #
            # WORKING:
            # INFO: Parsing report '/home/ck/workspace-python/simple-sonar-test/coverage-reports/coverage-app.xml'
            # DEBUG: Saving coverage measures for file 'app/services.py'
            # DEBUG: Saving coverage measures for file 'app/test_services.py'
            # DEBUG: Saving coverage measures for file 'app/__init__.py'
            # INFO; Sensor Cobertura Sensor for Python coverage [python] (done) | time=23ms
            #
            # NOT WORKING:
            # INFO: Parsing report '/home/ck/workspace-python/simple-sonar-test/coverage-reports/coverage-app.xml'
            # INFO; Sensor Cobertura Sensor for Python coverage [python] (done) | time=23ms
            cmd = getattr(settings, 'SONAR_SCANNER_CMD', 'sonar-scanner')
            print(cmd)
            subprocess.run(cmd.split(' '), env=sub_env)

        except Exception as e:
            self.stdout.write(self.style.ERROR(e))
            traceback.print_exc()
            raise CommandError('fail')
