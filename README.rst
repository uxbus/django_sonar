======
DJANGO SONAR
======
written to ease usage of sonarqube for projects using django, pytest, pytest-cov. there have been some not very obvious problems in getting test- and coverage-data into sonarqube (which are very important to get a good quality-gate).


Quick start
-----------
1. install the package

2. add app to settings like:

INSTALLED_APPS = (
    …,
    'sonar',
)

3. adopt settings:
  * optional SONAR_COVERAGE_SOURCE_TO_BASE_DIR replaces all <source> tags in coverage reports to point to settings.BASE_DIR (if True) or to the value if str
  * optional SONAR_SCANNER_CMD point to your local sonar-scanner executable
  * optional SONAR_TEST_REPORTS directory name
  * optional SONAR_COVERAGE_REPORTS directory name

 to point to your sonar-scanner, project and path

4. add a sonar.properties file (there is a sample in this repo)

5. run django-admin sonar